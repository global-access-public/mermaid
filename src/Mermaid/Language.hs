{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE NoMonomorphismRestriction #-}

module Mermaid.Language where

import Control.Monad.Trans (MonadTrans)
import Control.Monad.Trans.Free (FreeT (FreeT), liftF)
import Mermaid.Types
  ( GraphShape
  , LinkName
  , LinkStyle
  , NodeName
  , NodeShape
  , SubgraphName
  )
import Protolude

data MermaidF n m a where
  Node :: NodeShape -> NodeName -> (n -> a) -> MermaidF n m a
  Link :: LinkStyle -> Maybe LinkName -> n -> n -> a -> MermaidF n m a
  Subgraph :: Maybe SubgraphName -> Mermaid n m g -> ((n, g) -> a) -> MermaidF n m a
  Direction :: GraphShape -> a -> MermaidF n m a

instance Functor (MermaidF n m) where
  fmap f (Node ns nm g) = Node ns nm $ f . g
  fmap f (Link ns nm n1 n2 x) = Link ns nm n1 n2 $ f x
  fmap f (Subgraph nm m g) = Subgraph nm m $ f . g
  fmap f (Direction gs x) = Direction gs $ f x

newtype Mermaid n m a = Mermaid {unMermaid :: FreeT (MermaidF n m) m a}
  deriving (Functor, Monad, Applicative, MonadFail, MonadIO)

instance MonadTrans (Mermaid n) where
  lift = Mermaid . lift

node :: Monad m => NodeShape -> NodeName -> Mermaid n m n
node ns nm = Mermaid $ liftF (Node ns nm identity)

link :: Monad m => LinkStyle -> Maybe LinkName -> n -> n -> Mermaid n m ()
link ns nm n1 n2 = Mermaid $ liftF (Link ns nm n1 n2 ())

subgraph :: Monad m => Maybe SubgraphName -> Mermaid n m g -> Mermaid n m (n, g)
subgraph sn m = Mermaid $ liftF (Subgraph sn m identity)

direction :: Monad m => GraphShape -> Mermaid n m ()
direction gs = Mermaid $ liftF (Direction gs ())

newtype NodeID = NodeID Int deriving newtype (Show, Eq, Ord, Enum, Num)

type MermaidI m = Mermaid NodeID m
