{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Mermaid.Render where

import Control.Monad.Trans.Free
  ( FreeF (Free, Pure)
  , FreeT (runFreeT)
  )
import qualified Data.Text as T
import Mermaid.Language
  ( Mermaid (Mermaid)
  , MermaidF (Link, Node, Subgraph, Direction)
  , MermaidI
  , NodeID (..)
  )
import Mermaid.Types
  ( ArrowShape (Bird, Cross, Dot)
  , GraphShape (..)
  , LinkArrow (Arrow, BiArrow, NoArrow)
  , LinkLenght (L1, L2, L3)
  , LinkLine (Dotted, Solid, Thick)
  , LinkName (linkNameText)
  , LinkStyle (LinkStyle)
  , NodeName (..)
  , NodeShape (..)
  , SubgraphName (..)
  )
import Protolude hiding (L1, L2, L3)
import Text.InterpolatedString.QM (qms)


quote :: (Semigroup a, IsString a) => a -> a
quote x = "\"" <> x <> "\""

renderNode :: NodeShape -> NodeName -> NodeID -> (Text, NodeID, NodeID)
renderNode shape (NodeName (quote -> name)) n@(NodeID top) = (,,)
  do
    case shape of
      Round -> [qms|{top}({name})|]
      Stadium -> [qms|{top}([{name}])|]
      Circle -> [qms|{top}(({name}))|]
      Database -> [qms|{top}[({name})]|]
      Subroutine -> [qms|{top}[[{name}]]|]
      Asymmetric -> [qms|{top}>{name}]|]
      Rhombus -> [qms|{top}\{{name}}|]
      Hexagon -> [qms|{top}\{\{{name}}}|]
      ParallelogramR -> [qms|{top}[/{name}/]|]
      ParallelogramL -> [qms|{top}[\\{name}\\]|]
      TrapezoidB -> [qms|{top}[/{name}\\]|]
      TrapezoidT -> [qms|{top}[\\{name}/]|]
  do n
  do NodeID $ succ top

renderLink :: LinkStyle -> Maybe LinkName -> NodeID -> NodeID -> Text
renderLink ls (fmap linkNameText -> name) n1 n2 = [qms|{n1}{l}{t}{n2}|]
  where
    t :: Text = maybe "" (\x -> [qms||{x}||]) name
    l :: Text = case ls of
      LinkStyle Solid NoArrow L1 -> "---"
      LinkStyle Solid NoArrow L2 -> "----"
      LinkStyle Solid NoArrow L3 -> "-----"
      LinkStyle Thick NoArrow L1 -> "==="
      LinkStyle Thick NoArrow L2 -> "===="
      LinkStyle Thick NoArrow L3 -> "====="
      LinkStyle Dotted NoArrow L1 -> "-.-"
      LinkStyle Dotted NoArrow L2 -> "-..-"
      LinkStyle Dotted NoArrow L3 -> "-...-"
      LinkStyle Solid (Arrow s) L1 -> ce s "--"
      LinkStyle Solid (Arrow s) L2 -> ce s "---"
      LinkStyle Solid (Arrow s) L3 -> ce s "----"
      LinkStyle Thick (Arrow s) L1 -> ce s "=="
      LinkStyle Thick (Arrow s) L2 -> ce s "==="
      LinkStyle Thick (Arrow s) L3 -> ce s "===="
      LinkStyle Dotted (Arrow s) L1 -> ce s "-.-"
      LinkStyle Dotted (Arrow s) L2 -> ce s "-..-"
      LinkStyle Dotted (Arrow s) L3 -> ce s "-...-"
      LinkStyle Solid (BiArrow s) L1 -> cse s "--"
      LinkStyle Solid (BiArrow s) L2 -> cse s "---"
      LinkStyle Solid (BiArrow s) L3 -> cse s "----"
      LinkStyle Thick (BiArrow s) L1 -> cse s "=="
      LinkStyle Thick (BiArrow s) L2 -> cse s "==="
      LinkStyle Thick (BiArrow s) L3 -> cse s "===="
      LinkStyle Dotted (BiArrow s) L1 -> cse s "-.-"
      LinkStyle Dotted (BiArrow s) L2 -> cse s "-..-"
      LinkStyle Dotted (BiArrow s) L3 -> cse s "-...-"
    ce Bird xs = xs <> ">"
    ce Dot xs = xs <> "o"
    ce Cross xs = xs <> "x"
    cse Bird xs = "<" <> xs <> ">"
    cse Dot xs = "o" <> xs <> "o"
    cse Cross xs = "x" <> xs <> "x"

renderSubgraphM
  :: Monad m
  => Maybe SubgraphName
  -> MermaidI m g
  -> NodeID
  -> m ([Text], (NodeID, g), NodeID)
renderSubgraphM mname content n@(NodeID top) = do
  (top', g, contentLines) <- renderMermaidM (succ n) content
  pure $ (,,)
    do name  : contentLines <> ["end"]
    do (n, g)
    do top'
  where name = maybe [qms|subgraph {top}|]  (\(SubgraphName (quote -> name')) -> [qms|subgraph {top}[{name'}]|]) mname

renderMermaidM :: Monad m => NodeID -> MermaidI m g -> m (NodeID, g, [Text])
renderMermaidM top (Mermaid x) = do
  y <- runFreeT x
  case y of
    Pure g -> pure (top, g, [])
    Free g -> case g of
      Node ns nm f ->
        let (t, n, top') = renderNode ns nm top
         in second (t :) <$> renderMermaidM top' (Mermaid $ f n)
      Link ns nm n1 n2 x ->
        second (renderLink ns nm n1 n2 :) <$> renderMermaidM top (Mermaid x)
      Subgraph sn m f -> do
        (xs, ls, top') <- renderSubgraphM sn m top
        second (xs <>) <$> renderMermaidM top' (Mermaid $ f ls)
      Direction gs x -> let 
          t = "direction " <> show gs 
          in second (t :) <$> renderMermaidM top (Mermaid x)

graph
  :: (MonadFail m)
  => GraphShape
  -> MermaidI m ()
  -> m Text
graph shape content =
  T.intercalate "\n"
    . (:)
      do "flowchart " <> show shape
    . fmap
      do ("  " <>)
    . (\(_, _, ts) -> ts)
    <$> renderMermaidM 0 content
