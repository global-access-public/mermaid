{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Mermaid.Example where

import Mermaid
  ( ArrowShape (Bird, Cross)
  , GraphShape (LR)
  , LinkArrow (Arrow, BiArrow, NoArrow)
  , LinkLenght (L1, L3)
  , LinkLine (..)
  , LinkStyle (LinkStyle)
  , MermaidI
  , NodeShape (Circle, Database, Round, Stadium, TrapezoidT)
  , SubgraphName (SubgraphName)
  , graph
  , link
  , node
  , subgraph, NodeName (NodeName), LinkName (LinkName)
  )
import Protolude hiding (L1, L2, L3, link)
import Deriving.Show.Simple (WrapSimple(WrapSimple))

main :: IO ()
main = do 
  putText "```mermaid"
  graph LR eden >>= putText
  putText "```"

allLinks :: LinkLine -> [LinkStyle]
allLinks l = do
  -- l <- [Solid .. Thick]
  a <-
    NoArrow : do
      wa <- [Arrow, BiArrow]
      wa <$> [Bird .. Cross]
  LinkStyle l a <$> [L1 .. L3]

solid :: LinkStyle
solid = LinkStyle Solid (Arrow Bird) L1

dotted :: LinkStyle
dotted = LinkStyle Dotted (Arrow Bird) L1

eden :: (Monad m, MonadFail m) => MermaidI m ()
eden = do
  subgraph (Just "node types") do
    forM_ [Round .. TrapezoidT] $ \shape -> node shape $ NodeName $ show shape
  forM_ [Solid .. Thick] $ \ll ->
    subgraph (Just ("lines, " <> SubgraphName (show ll))) do
      forM_ (allLinks ll) $ \l -> do
        n1 <- node Circle "N1"
        n2 <- node Circle "N2"
        link l (Just $  LinkName $ "\"" <>show (WrapSimple l) <> "\"") n1 n2

  void $ subgraph (Just "simple life") do
    x <- node Round "paolo"
    (g, (s, h, b)) <-
      subgraph (Just "eden garden") do
        s <- node Stadium "sara"
        h <- node Database "haskell"
        b <- node Circle "blu"
        link solid (Just "envies") s h
        link solid (Just "envies") s b
        pure (s, h, b)
    link dotted (Just "loves") x s
    link dotted (Just "thinks of") x h
    link dotted (Just "stays with") x b
    link solid (Just "dreams of") x g
