{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DeriveGeneric #-}

module Mermaid.Types where

import Protolude

data NodeShape
  = Round
  | Stadium
  | Circle
  | Database
  | Subroutine
  | Asymmetric
  | Rhombus
  | Hexagon
  | ParallelogramR
  | ParallelogramL
  | TrapezoidB
  | TrapezoidT
  deriving (Enum, Show, Eq)

data LinkLine = Solid | Dotted | Thick
  deriving (Enum, Show, Eq, Generic)

data LinkArrow = NoArrow | Arrow ArrowShape | BiArrow ArrowShape
  deriving ( Show, Eq, Generic)

data ArrowShape = Bird | Dot | Cross
  deriving (Enum,Show,Eq, Generic)

data LinkLenght = L1 | L2 | L3
  deriving (Enum, Show,Eq, Generic)

data LinkStyle = LinkStyle
  { link_linkLine :: LinkLine
  , link_linkArrow :: LinkArrow
  , link_linkLength :: LinkLenght
  }
  deriving ( Show, Eq, Generic)

newtype NodeName = NodeName Text deriving newtype (Show, Eq, IsString, Semigroup)

newtype LinkName = LinkName {linkNameText :: Text}
  deriving newtype (Show, Eq, IsString, Semigroup)

newtype SubgraphName = SubgraphName Text deriving newtype (Show, Eq, IsString, Semigroup)



data GraphShape = TB | BT | LR | RL 
  deriving ( Show, Eq, Enum)
