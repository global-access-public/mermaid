# mermaid

DSL to write mermaid graphs

this is the result if you run mermeid-exe (press Display button on the bottom if it's not rendered straight away)
```mermaid
flowchart LR
  subgraph 0[nodes]
  1(Round)
  2([Stadium])
  3((Circle))
  4[(Database)]
  5[[Subroutine]]
  6>Asymmetric]
  7{Rhombus}
  8{{Hexagon}}
  9[/ParallelogramR/]
  10[\ParallelogramL\]
  11[/TrapezoidB\]
  12[\TrapezoidT/]
  end
  subgraph 13[lines, Solid]
  14((N1))
  15((N2))
  14---|"LinkStyle Solid NoArrow L1"|15
  16((N1))
  17((N2))
  16----|"LinkStyle Solid NoArrow L2"|17
  18((N1))
  19((N2))
  18-----|"LinkStyle Solid NoArrow L3"|19
  20((N1))
  21((N2))
  20-->|"LinkStyle Solid (Arrow Bird) L1"|21
  22((N1))
  23((N2))
  22--->|"LinkStyle Solid (Arrow Bird) L2"|23
  24((N1))
  25((N2))
  24---->|"LinkStyle Solid (Arrow Bird) L3"|25
  26((N1))
  27((N2))
  26--o|"LinkStyle Solid (Arrow Dot) L1"|27
  28((N1))
  29((N2))
  28---o|"LinkStyle Solid (Arrow Dot) L2"|29
  30((N1))
  31((N2))
  30----o|"LinkStyle Solid (Arrow Dot) L3"|31
  32((N1))
  33((N2))
  32--x|"LinkStyle Solid (Arrow Cross) L1"|33
  34((N1))
  35((N2))
  34---x|"LinkStyle Solid (Arrow Cross) L2"|35
  36((N1))
  37((N2))
  36----x|"LinkStyle Solid (Arrow Cross) L3"|37
  38((N1))
  39((N2))
  38<-->|"LinkStyle Solid (BiArrow Bird) L1"|39
  40((N1))
  41((N2))
  40<--->|"LinkStyle Solid (BiArrow Bird) L2"|41
  42((N1))
  43((N2))
  42<---->|"LinkStyle Solid (BiArrow Bird) L3"|43
  44((N1))
  45((N2))
  44o--o|"LinkStyle Solid (BiArrow Dot) L1"|45
  46((N1))
  47((N2))
  46o---o|"LinkStyle Solid (BiArrow Dot) L2"|47
  48((N1))
  49((N2))
  48o----o|"LinkStyle Solid (BiArrow Dot) L3"|49
  50((N1))
  51((N2))
  50x--x|"LinkStyle Solid (BiArrow Cross) L1"|51
  52((N1))
  53((N2))
  52x---x|"LinkStyle Solid (BiArrow Cross) L2"|53
  54((N1))
  55((N2))
  54x----x|"LinkStyle Solid (BiArrow Cross) L3"|55
  end
  subgraph 56[lines, Dotted]
  57((N1))
  58((N2))
  57-.-|"LinkStyle Dotted NoArrow L1"|58
  59((N1))
  60((N2))
  59-..-|"LinkStyle Dotted NoArrow L2"|60
  61((N1))
  62((N2))
  61-...-|"LinkStyle Dotted NoArrow L3"|62
  63((N1))
  64((N2))
  63-.->|"LinkStyle Dotted (Arrow Bird) L1"|64
  65((N1))
  66((N2))
  65-..->|"LinkStyle Dotted (Arrow Bird) L2"|66
  67((N1))
  68((N2))
  67-...->|"LinkStyle Dotted (Arrow Bird) L3"|68
  69((N1))
  70((N2))
  69-.-o|"LinkStyle Dotted (Arrow Dot) L1"|70
  71((N1))
  72((N2))
  71-..-o|"LinkStyle Dotted (Arrow Dot) L2"|72
  73((N1))
  74((N2))
  73-...-o|"LinkStyle Dotted (Arrow Dot) L3"|74
  75((N1))
  76((N2))
  75-.-x|"LinkStyle Dotted (Arrow Cross) L1"|76
  77((N1))
  78((N2))
  77-..-x|"LinkStyle Dotted (Arrow Cross) L2"|78
  79((N1))
  80((N2))
  79-...-x|"LinkStyle Dotted (Arrow Cross) L3"|80
  81((N1))
  82((N2))
  81<-.->|"LinkStyle Dotted (BiArrow Bird) L1"|82
  83((N1))
  84((N2))
  83<-..->|"LinkStyle Dotted (BiArrow Bird) L2"|84
  85((N1))
  86((N2))
  85<-...->|"LinkStyle Dotted (BiArrow Bird) L3"|86
  87((N1))
  88((N2))
  87o-.-o|"LinkStyle Dotted (BiArrow Dot) L1"|88
  89((N1))
  90((N2))
  89o-..-o|"LinkStyle Dotted (BiArrow Dot) L2"|90
  91((N1))
  92((N2))
  91o-...-o|"LinkStyle Dotted (BiArrow Dot) L3"|92
  93((N1))
  94((N2))
  93x-.-x|"LinkStyle Dotted (BiArrow Cross) L1"|94
  95((N1))
  96((N2))
  95x-..-x|"LinkStyle Dotted (BiArrow Cross) L2"|96
  97((N1))
  98((N2))
  97x-...-x|"LinkStyle Dotted (BiArrow Cross) L3"|98
  end
  subgraph 99[lines, Thick]
  100((N1))
  101((N2))
  100===|"LinkStyle Thick NoArrow L1"|101
  102((N1))
  103((N2))
  102====|"LinkStyle Thick NoArrow L2"|103
  104((N1))
  105((N2))
  104=====|"LinkStyle Thick NoArrow L3"|105
  106((N1))
  107((N2))
  106==>|"LinkStyle Thick (Arrow Bird) L1"|107
  108((N1))
  109((N2))
  108===>|"LinkStyle Thick (Arrow Bird) L2"|109
  110((N1))
  111((N2))
  110====>|"LinkStyle Thick (Arrow Bird) L3"|111
  112((N1))
  113((N2))
  112==o|"LinkStyle Thick (Arrow Dot) L1"|113
  114((N1))
  115((N2))
  114===o|"LinkStyle Thick (Arrow Dot) L2"|115
  116((N1))
  117((N2))
  116====o|"LinkStyle Thick (Arrow Dot) L3"|117
  118((N1))
  119((N2))
  118==x|"LinkStyle Thick (Arrow Cross) L1"|119
  120((N1))
  121((N2))
  120===x|"LinkStyle Thick (Arrow Cross) L2"|121
  122((N1))
  123((N2))
  122====x|"LinkStyle Thick (Arrow Cross) L3"|123
  124((N1))
  125((N2))
  124<==>|"LinkStyle Thick (BiArrow Bird) L1"|125
  126((N1))
  127((N2))
  126<===>|"LinkStyle Thick (BiArrow Bird) L2"|127
  128((N1))
  129((N2))
  128<====>|"LinkStyle Thick (BiArrow Bird) L3"|129
  130((N1))
  131((N2))
  130o==o|"LinkStyle Thick (BiArrow Dot) L1"|131
  132((N1))
  133((N2))
  132o===o|"LinkStyle Thick (BiArrow Dot) L2"|133
  134((N1))
  135((N2))
  134o====o|"LinkStyle Thick (BiArrow Dot) L3"|135
  136((N1))
  137((N2))
  136x==x|"LinkStyle Thick (BiArrow Cross) L1"|137
  138((N1))
  139((N2))
  138x===x|"LinkStyle Thick (BiArrow Cross) L2"|139
  140((N1))
  141((N2))
  140x====x|"LinkStyle Thick (BiArrow Cross) L3"|141
  end
  subgraph 142[simple life]
  143(paolo)
  subgraph 144[eden garden]
  145([sara])
  146[(haskell)]
  147((blu))
  145-->|envies|146
  145-->|envies|147
  end
  143-.->|loves|145
  143-.->|thinks of|146
  143-.->|stays with|147
  143-->|dreams of|144
  end
```

